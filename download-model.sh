#!/bin/sh

# Edite as configurações de importação
DATA_DIR=./model

mkdir -p $DATA_DIR 
wget -qO- -O $DATA_DIR/model_frcnn.zip https://www.dropbox.com/s/4aaumoidekk8t8s/model_frcnn.zip
unzip -o $DATA_DIR/model_frcnn.zip -d model
rm $DATA_DIR/model_frcnn.zip
